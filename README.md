# DesafioLetscode

Projeto feito para o Desafio Técnico - Frontend 

## Requisitos

- Angular 15 instalado
- Node v14+

## Passos para rodar o frontend

Acesse o diretório do frontend
Instale os pacotes NPM: `npm install`
Execute o comando `ng serve` e abra em seu browser na seguinte URL: `http://localhost:4200/`

para rodar os testes basta executar o seguinte comando: `ng test`


## Passos para rodar o backend

Acesse o diretório do frontend
Instale os pacotes NPM: `npm install`
Roda o comando `npm run server` e abra em seu browser na seguinte URL: `http://localhost:4200/`

## Observações 

- Eu mudei a porta de execução do backend de 5000 para 3000 pois tive que fazer esse teste no meu computador corporativo e já estava sendo utilizada a porta 5000.
- Para mover os cards entre as colunas to-do, doing, done eu utilizei o método drag/drop. Basta arrastar os cards para move-los.