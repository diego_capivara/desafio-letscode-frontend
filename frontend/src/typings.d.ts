declare module '@ckeditor/ckeditor5-build-classic' {
  const ClassicEditorBuild: any;

  export = ClassicEditorBuild;
}

declare module '@ckeditor/ckeditor5-watchdog' {
  const Watchdog: any;
  const EditorWatchdog: any;
  const ContextWatchdog: any;

  export = Watchdog;
}