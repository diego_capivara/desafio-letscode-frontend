import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppContainerComponent } from './app-container.component';
import { AppHeaderModule } from '../components/app-header/app-header.module';
import { RouterModule } from '@angular/router';
import { AppContainerRoutes } from './app-container.routing';

@NgModule({
  imports: [
    CommonModule,
    AppHeaderModule,
    RouterModule,
    AppContainerRoutes
  ],
  declarations: [AppContainerComponent],
  exports: [AppContainerComponent],
})
export class AppContainerModule { }
