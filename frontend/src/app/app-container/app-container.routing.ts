import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {  
    path: '',
    children: [
      {
        path: 'kanban',
        loadChildren: () => import('../pages/kanban-page/kanban-page.module').then(m => m.KanbanPageModule)
      },
      { path: '**', redirectTo: '/kanban' },
    ]
  },
];

export const AppContainerRoutes = RouterModule.forChild(routes);
