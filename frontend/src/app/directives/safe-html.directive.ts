import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import * as DOMPurify from 'dompurify';

@Pipe({ name: 'safeHtml' })
export class SafeHtml implements PipeTransform {

    constructor(private sanitizer: DomSanitizer) { }

    transform(html: string) {
        html = DOMPurify.sanitize(html);
        return this.sanitizer.bypassSecurityTrustHtml(html);
    }
} 