import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { mergeMap, catchError } from 'rxjs/operators';
import { AuthService } from 'app/services/auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const allowedUrls = ['http://localhost:3000/login'];
    const token = this.authService.getToken();
    console.log('token', token);
    if (allowedUrls.includes(req.url)) {
      return next.handle(req);

    }
    if (!token || allowedUrls.includes(req.url)) {
      return this.loginAndNext(req, next);
    }
    const decodeToken = this.parseJwt(token);
    const isExpired = this.isExpiredJwt(decodeToken);
    if (!isExpired) {
      const dupReq = req.clone({
        headers: req.headers.set('Authorization', `Bearer ${token}`),
      });
      return next.handle(dupReq);
    } else {
      return this.loginAndNext(req, next);
    }
  }

  loginAndNext(req: HttpRequest<any>, next: HttpHandler) {
    return this.authService.login().pipe(
      mergeMap((token: string) => {
        if (!token) {
          return next.handle(req);
        }
        this.authService.saveToken(token);
        const dupReq = req.clone({
          headers: req.headers
            .set('Authorization', `Bearer ${token}`)
            .set('Accept', 'application/json')
            .set('Content-Type', 'application/json')
        });
        return next.handle(dupReq);
      })
    );
  }

  isExpiredJwt(token) {
    if (!token || (token && !token.exp)) {
      return true;
    }
    const exp = token.exp;
    if (Date.now() >= exp * 1000) {
      return true;
    }
    return false;
  }

  parseJwt(token) {
    let jsonPayload;
    try {
      const base64Url = token.split('.')[1];
      const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
      jsonPayload = decodeURIComponent(
        atob(base64)
          .split('')
          .map((c) => {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
          })
          .join('')
      );
    } catch (err) {
      return null;
    }

    return JSON.parse(jsonPayload);
  }
}
