import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KanbanPageComponent } from './kanban-page.component';
import { KanbanPageRoutes } from './kanban-page.routing';
import { KanbanCardItemModule } from 'app/components/kanban-card-item/kanban-card-item.module';
import { KanbanAddCardFormModule } from 'app/components/kanban-add-card-form/kanban-add-card-form.module';
import { DragDropModule } from '@angular/cdk/drag-drop';

@NgModule({
  imports: [
    CommonModule,
    KanbanPageRoutes,
    KanbanCardItemModule,
    KanbanAddCardFormModule,
    DragDropModule
  ],
  declarations: [KanbanPageComponent]
})
export class KanbanPageModule { }
