import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { KanbanCardItemComponent } from 'app/components/kanban-card-item/kanban-card-item.component';
import { CardModel } from 'app/models/card.model';
import { CardService } from 'app/services/card.service';
import { map, Observable, Subscription, tap } from 'rxjs';

export interface CardsByList {
  toDo: CardModel[];
  inProgress: CardModel[];
  done: CardModel[];
}

@Component({
  selector: 'app-kanban-page',
  templateUrl: './kanban-page.component.html',
  styleUrls: ['./kanban-page.component.scss']
})
export class KanbanPageComponent implements OnInit, OnDestroy {

  @ViewChild(KanbanCardItemComponent) kanbanCardItemComponent!: KanbanCardItemComponent;

  subscriptions: Subscription[] = [];
  allowDragAndDrop: boolean = true;
  cards$: Observable<CardsByList>;

  constructor(
    private cardService: CardService,
  ) { }

  ngOnInit() {
    this.getCards();
  }

  ngOnDestroy(): void {
    this.subscriptions.map(subscription => subscription.unsubscribe());
  }

  test(event) {
    alert(event);
    this.allowDragAndDrop = !event;
  }

  getCards() {
    this.cards$ = this.cardService.getCards().pipe(
      map(cards => {
        return {
          toDo: cards.filter(card => card.lista === 'to-do'),
          inProgress: cards.filter(card => card.lista === 'in-progress'),
          done: cards.filter(card => card.lista === 'done'),
        }
      })
    );
  }

  drop(event, column: string) {
    let cardToBeMoved: CardModel = event.previousContainer.data[event.previousIndex];
    cardToBeMoved.lista = column;
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex,
      );
    }
    const moveCard$ = this.cardService.updateCards(cardToBeMoved.id, cardToBeMoved);
    this.subscriptions.push(moveCard$.subscribe());
  }

  deleteCard(id: string) {
    const editCard$ = this.cardService.deleteCard(id).pipe(
      tap(() => {
        this.getCards();
      })
    )
    this.subscriptions.push(editCard$.subscribe());
  }

  editCard(card: CardModel) {
    const editCard$ = this.cardService.updateCards(card.id, card).pipe(
      tap(() => {
        this.getCards();
        this.kanbanCardItemComponent.hideEditMode();
      })
    )
    this.subscriptions.push(editCard$.subscribe());
  }

  addCard(cardToAdd: CardModel) {
    const addCard$ = this.cardService.addCard(cardToAdd).pipe(
      tap(() => {
        this.getCards();
      })
    );
    this.subscriptions.push(addCard$.subscribe());
  }
}
