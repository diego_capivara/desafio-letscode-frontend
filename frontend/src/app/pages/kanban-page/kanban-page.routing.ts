import { Routes, RouterModule } from '@angular/router';
import { KanbanPageComponent } from './kanban-page.component';

const routes: Routes = [
  {  
    path: '',
    component: KanbanPageComponent
  },
];

export const KanbanPageRoutes = RouterModule.forChild(routes);
