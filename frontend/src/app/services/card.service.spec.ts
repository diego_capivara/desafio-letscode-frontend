import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { CardService } from './card.service';
import { CardModel } from 'app/models/card.model';

describe('CardService', () => {
  let service: CardService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [CardService]
    });
    service = TestBed.inject(CardService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should add a card', () => {
    const cardToAdd: CardModel = {
      id: '1',
      titulo: 'Teste',
      conteudo: 'Conteudo de teste',
      lista: 'Lista de teste'
    };

    service.addCard(cardToAdd).subscribe((response) => {
      expect(response).toEqual(cardToAdd);
    });

    const req = httpMock.expectOne('http://localhost:3000/cards/');
    expect(req.request.method).toBe('POST');
    req.flush(cardToAdd);
  });

  it('should get cards', () => {
    const cards: CardModel[] = [
      {
        id: '1',
        titulo: 'Teste 1',
        conteudo: 'Conteudo de teste 1',
        lista: 'Lista de teste 1'
      },
      {
        id: '2',
        titulo: 'Teste 2',
        conteudo: 'Conteudo de teste 2',
        lista: 'Lista de teste 2'
      }
    ];

    service.getCards().subscribe((response) => {
      expect(response).toEqual(cards);
    });

    const req = httpMock.expectOne('http://localhost:3000/cards/');
    expect(req.request.method).toBe('GET');
    req.flush(cards);
  });

  it('should update a card', () => {
    const cardToEdit: CardModel = {
      id: '1',
      titulo: 'Teste editado',
      conteudo: 'Conteudo de teste editado',
      lista: 'Lista de teste editado'
    };

    service.updateCards('1', cardToEdit).subscribe((response) => {
      expect(response).toEqual(cardToEdit);
    });

    const req = httpMock.expectOne('http://localhost:3000/cards/1');
    expect(req.request.method).toBe('PUT');
    req.flush(cardToEdit);
  });

  it('should delete a card', () => {
    const id = '1';

    service.deleteCard(id).subscribe();

    const req = httpMock.expectOne(`http://localhost:3000/cards/${id}`);
    expect(req.request.method).toBe('DELETE');
    req.flush({});
  });
});