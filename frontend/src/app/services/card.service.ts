import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CardModel } from 'app/models/card.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CardService {

  constructor(private http: HttpClient) { }

  addCard(cardToAdd: CardModel) {
    return this.http.post('http://localhost:3000/cards/', cardToAdd);
  }

  getCards():Observable<CardModel[]> {
    return this.http.get<CardModel[]>('http://localhost:3000/cards/');
  }

  updateCards(id: string, cardToEdit: CardModel) {
    return this.http.put(`http://localhost:3000/cards/${id}`, cardToEdit);
  }

  deleteCard(id: string) {
    return this.http.delete(`http://localhost:3000/cards/${id}`);
  }

}

