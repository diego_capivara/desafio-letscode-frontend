import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { AuthService } from './auth.service';

describe('AuthService', () => {
  let authService: AuthService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AuthService]
    });

    authService = TestBed.inject(AuthService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(authService).toBeTruthy();
  });

  it('should return token from local storage', () => {
    localStorage.setItem('token', 'testToken');
    const token = authService.getToken();
    expect(token).toBe('testToken');
  });

  it('should save token to local storage', () => {
    authService.saveToken('testToken');
    const token = localStorage.getItem('token');
    expect(token).toBe('testToken');
  });

  it('should make a POST request to login and return a string', () => {
    const mockResponse = 'testToken';
    authService.login().subscribe((token: string) => {
      expect(token).toEqual(mockResponse);
    });

    const req = httpMock.expectOne('http://localhost:3000/login');
    expect(req.request.method).toBe('POST');
    req.flush(mockResponse);
  });
});