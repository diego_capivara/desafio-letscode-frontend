import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KanbanAddCardFormComponent } from './kanban-add-card-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    CKEditorModule
  ],
  declarations: [KanbanAddCardFormComponent],
  exports: [KanbanAddCardFormComponent],
})
export class KanbanAddCardFormModule { }
