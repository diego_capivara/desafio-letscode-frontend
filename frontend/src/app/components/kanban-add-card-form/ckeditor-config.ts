export const ckeditorConfig  = { 
  toolbar: [ 'heading', '|', 'bold', 'italic', 'bulletedList', 'numberedList' ],
  placeholder: 'Escreva o conteúdo do card aqui...'
};