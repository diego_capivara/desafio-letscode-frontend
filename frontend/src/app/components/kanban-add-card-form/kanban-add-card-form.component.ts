import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewEncapsulation,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { CardModel } from 'app/models/card.model';
import { ckeditorConfig } from './ckeditor-config';

@Component({
  selector: 'app-kanban-add-card-form',
  templateUrl: './kanban-add-card-form.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./kanban-add-card-form.component.scss'],
})
export class KanbanAddCardFormComponent implements OnInit {
  @Output() onSubmit: EventEmitter<CardModel> = new EventEmitter();
  @Output() onEdit: EventEmitter<CardModel> = new EventEmitter();

  @Input() lista: string;
  @Input() card: CardModel;
  editorConfig = ckeditorConfig;

  form: FormGroup;
  public Editor = ClassicEditor;
  editor;
  public onReady(editor) {
    this.editor = editor;
    if (this.card) {
      this.editor.setData(this.card.conteudo);
    }
  }

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.form = this.fb.group({
      titulo: [null, Validators.compose([Validators.required])],
      conteudo: [null, Validators.compose([Validators.required])],
      lista: [this.lista],
    });
    if (this.card) {
      console.log('card', this.card);
      this.form.patchValue(this.card);
    }
  }

  addCard(event) {
    if (!this.form.valid) {
      return;
    }
    const data: CardModel = this.form.value;
    this.onSubmit.emit(data);
  }

  editCard(event) {
    if (!this.form.valid) {
      return;
    }
    const data: CardModel = { id: this.card.id, ...this.form.value };
    this.onEdit.emit(data);
  }

  onCkeditorChanges(event): void {
    const data = event.editor.getData();
    this.form.get('conteudo')?.setValue(data);
  }
}
