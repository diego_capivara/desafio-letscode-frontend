import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { CardModel } from 'app/models/card.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-kanban-card-item',
  templateUrl: './kanban-card-item.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./kanban-card-item.component.scss']
})
export class KanbanCardItemComponent implements OnInit {

  @Input() card: CardModel;
  @Output() onDelete: EventEmitter<string> = new EventEmitter();
  @Output() onEdit: EventEmitter<CardModel> = new EventEmitter();
  @Output() editModeEnabled: EventEmitter<boolean> = new EventEmitter();

  editMode: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  showEditMode() {
    this.editMode = true;
    this.editModeEnabled.emit(this.editMode);
  }

  hideEditMode() {
    this.editMode = false;
    this.editModeEnabled.emit(this.editMode);
  }

  editCard(cardToBeEdited: CardModel) {
    this.editMode = true;
    this.onEdit.emit(cardToBeEdited);
  }

  delete() {
    Swal.fire({
      title: 'Você deseja remover esse card?',
      text: 'Essa ação não poderá ser desfeita',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Sim',
      cancelButtonText: 'Cancelar'
    }).then(result => {
      if (result.isConfirmed) {
        this.onDelete.emit(this.card.id);
      }
    });
  }

}
