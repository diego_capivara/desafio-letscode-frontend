import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KanbanCardItemComponent } from './kanban-card-item.component';
import { KanbanAddCardFormModule } from '../kanban-add-card-form/kanban-add-card-form.module';
import { SafeHtml } from 'app/directives/safe-html.directive';

@NgModule({
  imports: [
    CommonModule,
    KanbanAddCardFormModule
  ],
  declarations: [KanbanCardItemComponent, SafeHtml],
  exports: [KanbanCardItemComponent],
})
export class KanbanCardItemModule { }
