import { CommonModule } from '@angular/common';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SafeHtml } from 'app/directives/safe-html.directive';
import { CardModel } from 'app/models/card.model';
import { KanbanAddCardFormModule } from '../kanban-add-card-form/kanban-add-card-form.module';
import { KanbanCardItemComponent } from './kanban-card-item.component';

describe('KanbanCardItemComponent', () => {
  let component: KanbanCardItemComponent;
  let fixture: ComponentFixture<KanbanCardItemComponent>;

  const card: CardModel = {
    id: '1',
    titulo: 'Título do Card',
    conteudo: 'Conteúdo do Card',
    lista: 'lista1'
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        CommonModule,
        KanbanAddCardFormModule
      ],
      declarations: [KanbanCardItemComponent, SafeHtml],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KanbanCardItemComponent);
    component = fixture.componentInstance;
    component.card = card;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should enable edit mode when clicking on "Editar" button', () => {
    const editButton = fixture.nativeElement.querySelector('button.text-gray-500');
    editButton.click();
    expect(component.editMode).toBe(true);
  });
});